<a name="0.1.6"></a>

## [0.1.6](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/compare/v0.1.5...v0.1.6) (2018-02-15)

### Bug Fixes

* turn into a function ([2f82367](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/commit/2f82367))

<a name="0.1.5"></a>

## [0.1.5](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/compare/v0.1.4...v0.1.5) (2018-02-14)

### Features

* add transform-object-rest-spread ([55bc8b3](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/commit/55bc8b3))

<a name="0.1.4"></a>

## [0.1.4](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/compare/v0.1.3...v0.1.4) (2018-01-09)

### Bug Fixes

* another attempt at getting the test environment right ([e9807c1](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/commit/e9807c1))

<a name="0.1.3"></a>

## [0.1.3](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/compare/v0.1.2...v0.1.3) (2018-01-09)

### Bug Fixes

* use CommonJS module syntax for test environment (e.g. Jest) ([9df7a25](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/commit/9df7a25))

<a name="0.1.2"></a>

## [0.1.2](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/compare/v0.1.1...v0.1.2) (2018-01-03)

### Bug Fixes

* remove env option and replace it with actual JS logic ([5997d6e](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/commit/5997d6e))

<a name="0.1.1"></a>

## 0.1.1 (2018-01-03)

### Features

* first attempt at implementing the presets ([c34cc5c](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/commit/c34cc5c))

<a name="0.1.0"></a>

# 0.1.0 (2018-01-03)

### Features

* first attempt at implementing the presets ([c34cc5c](https://gitlab.com/ta-interaktiv/modules/babel-preset-react-component/commit/c34cc5c))
