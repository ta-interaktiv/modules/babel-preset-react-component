module.exports = function () {
  // Define in what environment we are
  var env = process.env.BABEL_ENV || process.env.NODE_ENV || 'default'

  var presets = [require('babel-preset-react')]
  var plugins = [
    require('babel-plugin-transform-class-properties'),
    require('babel-plugin-transform-object-rest-spread')
  ]

  if (env === 'test') {
    presets.push([require('babel-preset-env'), { targets: { node: true } }])
  }

  // Default is CommonJS
  if (env === 'default' || env === 'commonjs') {
    presets.push([
      require('babel-preset-env'),
      { modules: 'commonjs', useBuiltIns: false }
    ])
  }

  // For modern ECMA Script
  if (env === 'es') {
    presets.push([
      require('babel-preset-env'),
      { modules: false, useBuiltIns: false }
    ])
  }

  return {
    presets: presets,
    plugins: plugins
  }
}
