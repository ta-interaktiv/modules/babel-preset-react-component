# Babel Preset for Module Development

A simple babel preset for developing reusable React components.

## Installation

```bash
yarn add @ta-interaktiv/babel-preset-react-component --dev
```

Add to `.babelrc`:

```json
{
  "presets": ["@ta-interaktiv/babel-preset-react-component"],
  "env": {
    "test": {
      "presets": ["@ta-interaktiv/babel-preset-react-component"]
    }
  }
}
```
